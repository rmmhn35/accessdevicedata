import collections
import xlrd
import re
from ciscoconfparse import CiscoConfParse
from ciscoconfparse.ccp_util import IPv4Obj


search_patterns = {'run': ['show run', '#show int status'],
                   'int_status': ['show int status', '#show cdp nei detail'],
                   'cdp': ['show cdp nei detail', '#show mac add'],
                   'mac': ['show mac add', '#!end_of_the_output'],
                   'version': ['show hardware', '#show run']}

devices = [{'device': 'GRYNYCS-FL3S-01', 'device_type': '4510'}, {'device': 'GRYNYCS-FL3S-02', 'device_type': '3560'}]


def list_to_dict(rlist):
    """
    :param rlist: ['alpha:1', 'beta:2', 'gamma:3']
    :return: {'alpha': '1', 'beta': '2', 'gamma': '3'}
    """
    return dict(map(lambda s: s.split(':'), rlist))


def sanitize_excel_data(file_path):
    """
    This function will read contents of the sheet and return the ordered Ordereddict of replacement_data & device_info
    :param file_path: "/mnt/hgfs/Devnet_share/Legacy_2_New.xlsx"
    :return: Output [[replacementdata],ordereddict(device_info)]
    """
    output = []
    replacement_data = []
    device_info = collections.OrderedDict()
    try:
        wb = xlrd.open_workbook(file_path)
        sheet = wb.sheet_by_index(0)
    except IOError:
        print "IO Error ! Not able to find the file specified !"
    except:
        print "Something else went wrong !"
    else:
        for i in range(1, sheet.nrows):
            key = ''
            old_slot = ''
            device_type = ''
            new_slot = ''
            new_switch = ''
            default_voice_vlan = ''
            default_data_vlan = ''
            row_data = sheet.row_values(i)
            if '3560' in str(int(row_data[1])) and '4506' in str(int(row_data[4])):
                key = str(row_data[0])
                old_slot = 'FastEthernet' + str(int(row_data[2])) + '/'
                new_switch = str(row_data[3])
                new_slot = 'Gig' + str(int(row_data[5])) + '/'
                device_type = str(int(row_data[1]))
                default_data_vlan = str(int(row_data[6]))
                default_voice_vlan = str(int(row_data[7]))
            elif '4510' in str(int(row_data[1])) and '4506' in str(int(row_data[4])):
                key = str(row_data[0])
                old_slot = 'GigabitEthernet' + str(int(row_data[2])) + '/'
                new_switch = str(row_data[3])
                new_slot = 'Gig' + str(int(row_data[5])) + '/'
                device_type = str(int(row_data[1]))
                default_data_vlan = str(int(row_data[6]))
                default_voice_vlan = str(int(row_data[7]))
            elif '3560' in str(int(row_data[1])) and '2960' in str(int(row_data[4])):
                key = str(row_data[0])
                old_slot = 'FastEthernet' + str(int(row_data[2])) + '/'
                new_switch = str(row_data[3])
                new_slot = 'Gig' + str(int(row_data[5])) + '/0/'
                device_type = str(int(row_data[1]))
                default_data_vlan = str(int(row_data[6]))
                default_voice_vlan = str(int(row_data[7]))
            elif '4510' in str(int(row_data[1])) and '2960' in str(int(row_data[4])):
                key = str(row_data[0])
                old_slot = 'GigabitEthernet' + str(int(row_data[2])) + '/'
                new_switch = str(row_data[3])
                new_slot = 'Gig' + str(int(row_data[5])) + '/0/'
                device_type = str(int(row_data[1]))
                default_data_vlan = str(int(row_data[6]))
                default_voice_vlan = str(int(row_data[7]))
            replacement_data.append([key, old_slot, new_switch, new_slot, default_data_vlan, default_voice_vlan])
            device_info[key] = device_type
        output.append(replacement_data)
        output.append(device_info)
        return output


def fetch_old_new_int(replacement_data):
    replace_dict = {}
    for l in replacement_data:
        new_int = l[2]+','+l[3]
        old_int = l[0]+','+l[1]
        replace_dict[old_int] = new_int
    return replace_dict


def replace_all(interface, replacement):
    """
    :param interface: interface: interface information of existing one prepended with hostname
    :param replacement: replacement information
    :return: interface: modified interface.
    """
    for old, new in replacement.iteritems():
        interface = interface.replace(old, new)
    return interface


def sanitize_data_capture(device):
    """
    for every device, this function will read the data capture file and return
    if capture file has right content or not.
    :param device: hostname
    :return: True if text file of capture file has output of sh cdp nei det,sh int des
    """
    output = {}
    try:
        path = "/mnt/hgfs/Devnet_share/{}.txt".format(device)
    except RuntimeError:
        print "Something wrong with the capture file !"
    else:
        parse = CiscoConfParse(path)
        search_commands = ['show hardware', 'show run', 'show int status', 'show cdp nei detail', 'show mac add']
        boolean_output = []
        for command in search_commands:
            find_cmd = parse.find_objects(command)
            yn = True if find_cmd else False
            boolean_output.append(yn)
        output[device] = boolean_output
        return output


def get_raw_output(device, output_type, patterns):
    path = '/mnt/hgfs/Devnet_share/Uploads/{}'.format(device)
    fh = open(path, 'r')
    all_data = fh.read()
    start_pattern = patterns[output_type][0]
    end_pattern = '\r\n' + device + patterns[output_type][1]
    start_index = re.search(start_pattern, all_data).start()
    end_index = re.search(end_pattern, all_data).start()
    stripped_data = all_data[start_index:end_index]
    fh.close()
    return stripped_data


def get_interfaces(config_file, device_type):
    """
    This function is to get the list of interfaces from config file
    :param config_file: config file as input
    :param device_type: 4510/3560/2960/3850
    :return: all interfaces of device
    """
    config_file = config_file.split('\r\n')
    parse = CiscoConfParse(config_file)
    if device_type != '4510':
        int_type = "^interface FastEthernet.*"
    else:
        int_type = "^interface GigabitEthernet.*"
    all_interfaces = parse.find_objects(int_type)
    return all_interfaces


def short_int(interface_long):
    """
    This function will take the long interface name and return short form of it
    :param interface_long: GigabitEthernet10/48
    :return: int_short: Gi10/48
    """
    int_short = 'None'
    if "GigabitEthernet" in interface_long:
        int_short = interface_long.replace('gabitEthernet', '')
    elif "FastEthernet" in interface_long:
        int_short = interface_long.replace('stEthernet', '')
    elif "Vlan" in interface_long:
        int_short = interface_long.replace('an', '')
    int_short = int_short.replace('interface ', '')
    return int_short


def get_interface_info(all_interfaces):
    """
    this function will take all_interfaces information and return data_vlan,voice_vlan,description,data_flat,voice_flag
    data_flag - I if the vlan string is picked from running config, else it would be 'D'
    :param all_interfaces: IOSCfgLine list of interfaces.
    :return: output dictionary, Key -  interface and value  - data_vlan,voice_vlan,description,data_flat,voice_flag
    """
    output = collections.OrderedDict()
    for interface in all_interfaces:
        interface_data = {}
        short_interface = short_int(interface.text)
        data_vlan = ''
        voice_vlan = ''
        data_flag = 'D'
        voice_flag = 'D'
        description = ''
        if interface.re_search_children(r'switchport access vlan '):
            data_flag = 'I'
        if interface.re_search_children(r'switchport voice vlan '):
            voice_flag = 'I'
        for line in interface.children:
            if 'switchport access vlan ' in line.text:
                data_vlan = line.re_sub(r'switchport access vlan ', r'')
            if "switchport voice vlan " in line.text:
                voice_vlan = line.re_sub(r'switchport voice vlan ', r'')
            if "description" in line.text:
                description = line.re_sub(r'description ', r'')
        if interface.re_search_children(r'switchport mode access'):
            sw_port_mode = 'Access'
        elif interface.re_search_children(r'switchport mode trunk'):
            sw_port_mode = 'Trunk'
        elif interface.re_search_children(r'switchport mode access') and interface.re_search_children(r'switchport mode trunk'):
            sw_port_mode = 'Trunk & Access'
        else:
            sw_port_mode = 'Might be Routed'
        interface = interface.text.replace('interface ', '')
        interface_data.update({"data_vlan": data_vlan})
        interface_data.update({"voice_vlan": voice_vlan})
        interface_data.update({"description": description})
        interface_data.update({"data_flag": data_flag})
        interface_data.update({"voice_flag": voice_flag})
        interface_data.update({"short_int": short_interface})
        interface_data.update({"sw_port_mode": sw_port_mode})
        output[interface] = interface_data
    return output


def parse_cdp_data(cdp_data_input):
    output = []
    cdp_data = cdp_data_input.split('\n-------------------------\r')
    # pop the first entry of sh cdp nei det command output
    cdp_data.pop(0)
    for line in cdp_data:
        # remove the unwanted commas in the Version tab
        line = line.replace(',', '\n')
        # removing the unwanted Entry Address tab
        line = re.sub(r'Entry.*', '', line)
        # removing the unwanted IPv6 Address tab
        line = re.sub(r'IPv6.*', '', line)
        # removing the junk which i dont want after Holdtime
        line = re.sub(r'Hold.*', r'', line, flags=re.DOTALL)
        # replacing the /r/n with /n
        line = line.replace("\r\n", "\n")
        # replacing the /r with none
        line = line.replace("\r", "")
        line = line.replace(' ', "")
        line = line.replace('(outgoingport)', '_outgoingport')
        linesplit = line.split('\n')
        # remove the empty string in list
        linesplit = filter(None, linesplit)
        output.append(dict(list_to_dict(linesplit)))
    return output


def merge_run_cdp_data(interface_run_data, interface_cdp_data):
    """
    function will merge the output information for both runn and cdp information
    :param interface_run_data:
    :param interface_cdp_data:
    :return:
    """
    for run_interface in interface_run_data.keys():
        for cdp_interface_data in interface_cdp_data:
            if run_interface in cdp_interface_data.values():
                interface_run_data[run_interface].update(cdp_interface_data)
    return interface_run_data


def parse_int_status(int_status_data, run_cdp_int_data):
    int_status_data = int_status_data.split('\r\n')
    int_status_data.pop(2)
    int_status_data.pop(1)
    int_status_data.pop(0)
    status = 'NA'
    for key in run_cdp_int_data.keys():
        sht_int = run_cdp_int_data[key]['short_int']
        for line in int_status_data:
            line_list = " ".join(line.split()).split()
            if sht_int == line_list[0]:
                if 'connected' in line_list:
                    status = 'connected'
                elif 'notconnect' in line_list:
                    status = 'notconnect'
                elif 'err-disabled' in line_list:
                    status = 'err-disabled'
        # print short_int, status
        run_cdp_int_data[key]['status'] = status
    return run_cdp_int_data


def parse_mac_data(mac_table_output, run_cdp_status_data, device_type):
    # remove tail if there is any.
    mac_table_split = ''
    mac_index_no = 4
    if device_type == '4510':
        junk = '-------+---------------+--------+---------------------+--------------------'
        mac_table_output = re.sub(r"Multicast Entries.*", '', mac_table_output, flags=re.DOTALL)
        mac_table_output = re.sub(r"show mac add.* port", '', mac_table_output, flags=re.DOTALL)
        mac_table_output = mac_table_output.replace(junk, '')
        mac_table_split = mac_table_output.split('\r\n')
        mac_table_split = filter(None, mac_table_split)
        mac_index_no = 4
    elif device_type == '3560':
        junk = "----    -----------       --------    -----"
        mac_table_output = re.sub(r"Total Mac .*", '', mac_table_output, flags=re.DOTALL)
        mac_table_output = re.sub(r"show mac add.* Port", '', mac_table_output, flags=re.DOTALL)
        mac_table_output = mac_table_output.replace(junk, '')
        mac_table_split = mac_table_output.split('\r\n')
        mac_table_split = filter(None, mac_table_split)
        mac_table_split.pop(0)
        mac_index_no = 3
    for key in run_cdp_status_data.keys():
        mac_info = []
        if device_type == '3560':
            search_key = run_cdp_status_data[key]['short_int']
        else:
            search_key = key
        for line in mac_table_split:
            line = " ".join(line.split()).split()
            if search_key == line[mac_index_no]:
                mac_info.append(line[1])
        run_cdp_status_data[key]['mac_data'] = mac_info
        # print key, search_key, mac_info
        run_cdp_status_data[key]['mac_data'] = ','.join(mac_info)
    return run_cdp_status_data


def unique_vlans_data(run_int_data):
    unique_vlans = []
    for key in run_int_data.keys():
        if (run_int_data[key]['data_vlan']) and run_int_data[key]['data_vlan'] not in unique_vlans:
            unique_vlans.append(run_int_data[key]['data_vlan'])
        if (run_int_data[key]['voice_vlan']) and run_int_data[key]['voice_vlan'] not in unique_vlans:
            unique_vlans.append(run_int_data[key]['voice_vlan'])
    return ','.join(unique_vlans)


def get_model(show_ver):
    if 'cisco WS-C4510' in show_ver:
        device_type = '4510'
    elif 'WS-C3560' in show_ver:
        device_type = '3560'
    else:
        device_type = None
    return device_type


def get_hostname(config_file):
    """
    This function is to get the list of interfaces from config file
    :param config_file: config file as input
    :return: all hostname of config
    """
    config_file = config_file.split('\r\n')
    parse = CiscoConfParse(config_file)
    hostname = parse.find_objects("^hostname ")[0].re_sub(r'hostname ', '')
    return hostname


def get_mgmt_ip(config_file):
    """
    This function is to get the list of interfaces from config file
    :param config_file: config file as input
    :return: all hostname of config
    """
    config_file = config_file.split('\r\n')
    parse = CiscoConfParse(config_file)
    interface_cmds = parse.find_objects(r"^interface ")
    for interface_cmd in interface_cmds:
        # ... read interface name and description
        # extract IP addresses if defined
        ipv4_regex = r"ip\saddress\s(\S+\s+\S+)"
        for cmd in interface_cmd.re_search_children(ipv4_regex):
            # ciscoconfparse provides a helper function for this task
            ipv4_addr = interface_cmd.re_match_iter_typed(ipv4_regex, result_type=IPv4Obj)
            ip = str(ipv4_addr.ip.exploded)+'/'+str(ipv4_addr.prefixlen)
            vlan = interface_cmd.replace('interface ', '')
            return vlan, ip


def get_new_interface_info(hostname, fetch_old_new_int_data, run_cdp_status_mac_data):
    for interface in run_cdp_status_mac_data.keys():
        old_int_data = hostname + ',' + interface
        new_int_data = replace_all(old_int_data, fetch_old_new_int_data)
        run_cdp_status_mac_data[interface]['new_sw_info'] = new_int_data.split(',')[0]
        run_cdp_status_mac_data[interface]['new_port_info'] = new_int_data.split(',')[1]
    return run_cdp_status_mac_data


def constructor(device_info, port_info):
    device = device_info['device']
    # device_type = device_info['device_type']
    sh_ver = get_raw_output(device, 'version', search_patterns)
    device_type = get_model(sh_ver)
    sh_run = get_raw_output(device, 'run', search_patterns)
    hostname = get_hostname(sh_run)
    ip_add_info = get_mgmt_ip(sh_run)
    sw_interfaces = get_interfaces(sh_run, device_type)
    run_int_data = get_interface_info(sw_interfaces)
    unique_vlan_info = unique_vlans_data(run_int_data)
    cdp_data = get_raw_output(device, 'cdp', search_patterns)
    cdp_int_data = parse_cdp_data(cdp_data)
    run_cdp_int_data = merge_run_cdp_data(run_int_data, cdp_int_data)
    int_status_data = get_raw_output(device, 'int_status', search_patterns)
    run_cdp_status_data = parse_int_status(int_status_data, run_cdp_int_data)
    mac_table_output = get_raw_output(device, 'mac', search_patterns)
    run_cdp_status_mac_data = parse_mac_data(mac_table_output, run_cdp_status_data, device_type)
    if port_info:
        file_location = '/mnt/hgfs/Devnet_share/Uploads/Legacy_2_New.xlsx'
        replacement_data = sanitize_excel_data(file_location)[0]
        replacement_dict = fetch_old_new_int(replacement_data)
        run_cdp_status_mac_new_sw_port_data = get_new_interface_info(hostname, replacement_dict, run_cdp_status_mac_data)
        device_output = dict(hostname=hostname, hostname_type=device_type,
                             interface_information=run_cdp_status_mac_new_sw_port_data,
                             unique_vlan=unique_vlan_info, ip_info=ip_add_info)
    else:
        device_output = dict(hostname=hostname, hostname_type=device_type,
                             interface_information=run_cdp_status_mac_data,
                             unique_vlan=unique_vlan_info, ip_info=ip_add_info)
    return device_output


"""



def retrive_new_interface(hostname, sw_interfaces, replacement_data):

for l in replacement_data:
...     old_int = l[0]+','+l[1]
...     new_int = l[2]+','+l[3]
...     print old_int,new_int
... 


def parse_legacy_new_data(run_cdp_status_mac_data, replacement_data):
    for key in run_cdp_status_mac_data.keys():



    config_files = FileField('Files')

            table = ItemTable(output['interface_information'].values())
                return render_template('output.html', output=output, table=table, title="Switch Information")
            
            
            
#constructor(devices)
        {{ form.hostname.label }}
        {{ form.hostname }}
        <br>
        {{ form.device_model.label}}
        {{ form.device_model}}
        <br>
    IPAddress = Col('IP Add')

    Platform = Col('Platform')
    Capabilities = Col('Capabilities')
    
<table>
{% for int,int_info in output.interface_information.iteritems() %}
    <TR>
       <TD class="c2">{{int}}</TD>
       <TD class="c2">{{int_info.sw_port_mode}}</TD>
       <TD class="c2">{{int_info.status}}</TD>
       <TD class="c2">{{int_info.data_vlan}}</TD>
       <TD class="c2">{{int_info.voice_vlan}}</TD>
        <TD class="c2">{{int_info.DeviceID}}</TD>
        <TD class="c2">{{int_info.IPaddress}}</TD>
        <TD class="c2">{{int_info.Platform}}</TD>
        <TD class="c2">{{int_info.Capabilities}}</TD>
       <TD class="c2">{{int_info.PortID_outgoingport}}</TD>
        <TD class="c2">{{int_info.data_flag}}</TD>
        <TD class="c2">{{int_info.voice_flag}}</TD>
        <TD class="c2">{{int_info.description}}</TD>
        <TD class="c2">{{int_info.mac_data}}</TD>
    </TR>
{% endfor %}
</table>



    

def parse_mac_data(mac_table_output,device_type):
    #remove tail if there is any.
    mac_table_split = ''
    mac_index_no = 4
    if device_type == '4510':
        junk = '-------+---------------+--------+---------------------+--------------------'
        mac_table_output = re.sub(r"Multicast Entries.*", '', mac_table_output, flags=re.DOTALL)
        mac_table_output = re.sub(r"show mac add.* port", '', mac_table_output, flags=re.DOTALL)
        mac_table_output = mac_table_output.replace(junk, '')
        mac_table_split = mac_table_output.split('\r\n')
        mac_table_split = filter(None, mac_table_split)
        mac_index_no = 4
    elif device_info == '3560':
        junk = "----    -----------       --------    -----"
        mac_table_output = re.sub(r"Total Mac .*", '', mac_table_output, flags=re.DOTALL)
        mac_table_output = re.sub(r"show mac add.* Port", '', mac_table_output, flags=re.DOTALL)
        mac_table_output = mac_table_output.replace(junk, '')
        mac_table_split = mac_table_output.split('\r\n')
        mac_table_split = filter(None, mac_table_split)
        mac_table_split.pop(0)
        mac_index_no = 3
    print mac_table_split,mac_index_no
        
        
        
show command output into a file:

ter len 0
show hardware
show run
show int status
show cdp nei detail
show mac add
!end_of_the_output
exit

short_int = run_cdp_int_data[key].get('short_int')


mac_table_output = re.sub(r"Multicast Entries.*", '', mac_table_output, flags = re.DOTALL)
mac_table_output = re.sub(r"show mac add.* port", '', mac_table_output, flags=re.DOTALL)

to remove empty list in lists = mac_table_split = filter(None, mac_table_split)
to remove unwanted spaces in the strigs for split = " ".join(line.split()).split()

def parse_int_status(int_status_data, run_cdp_int_data):
    int_status_data = int_status_data.split('\r\n')
    int_status_data.pop(2)
    int_status_data.pop(1)
    int_status_data.pop(0)
    status = 'NA'
    for key in run_cdp_int_data.keys():
        short_int = run_cdp_int_data[key]['short_int']
        for line in int_status_data:
            
            if short_int in line_list[0]:
                print line_list[1]


================================
get raw output from the file for respective 

sh_run = get_raw_output(device, 'run', search_patterns)
int_statu
s = get_raw_output(device, 'int_status' , search_patterns)
cdp_output = get_raw_output(device, 'cdp', search_patterns)
mac_table_output = get_raw_output(device, 'mac', search_patterns)

! Unique Vlans for the device
>>> for key in run_cdp_int_data.keys():
...     if (run_cdp_int_data[key]['data_vlan']) and run_cdp_int_data[key]['data_vlan'] not in unique_vlans:
...         unique_vlans.append(run_cdp_int_data[key]['data_vlan'])
...     if (run_cdp_int_data[key]['voice_vlan']) and run_cdp_int_data[key]['voice_vlan'] not in unique_vlans:
...         unique_vlans.append(run_cdp_int_data[key]['voice_vlan'])
... 
>>> unique_vlans = []
>>> for key in run_cdp_int_data.keys():
...     if (run_cdp_int_data[key]['data_vlan']) and run_cdp_int_data[key]['data_vlan'] not in unique_vlans:
...         unique_vlans.append(run_cdp_int_data[key]['data_vlan'])
...     if (run_cdp_int_data[key]['voice_vlan']) and run_cdp_int_data[key]['voice_vlan'] not in unique_vlans:
...         unique_vlans.append(run_cdp_int_data[key]['voice_vlan'])
... 
>>> unique_vlans
[' 321', ' 322', ' 3001', ' 3190', ' 67', ' 3014', ' 3030', ' 3009', ' 3080']



>>> def get_interface_info(all_interfaces):
...     output = []
...     for interface in all_interfaces:
...         short_interface = short_int(interface.text)
...         data_vlan = ''
...         voice_vlan = ''
...         data_flag = ''
...         voice_flag = ''
...         if interface.re_search_children(r'switchport access vlan '):
...             data_flag = 'I'
...         if interface.re_search_children(r'switchport voice vlan '):
...             voice_flag = 'I'
...         for line in interface.children:
...             if 'switchport access vlan ' in line.text:
...                 data_vlan = line.re_sub(r'switchport access vlan ', r'')
...             if "switchport voice vlan " in line.text:
...                 voice_vlan = line.re_sub(r'switchport voice vlan ', r'')
...         output_data = data_vlan + ',' + voice_vlan + ',' + data_flag + ',' + voice_flag + ',' + short_interface
...         output.append(output_data)
...     return output
... 


"""