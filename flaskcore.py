from flask import Flask, render_template, request
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SelectField, SubmitField
from werkzeug.utils import secure_filename
import os
from Summary_data import constructor
from flask_table import Table, Col


app = Flask(__name__)
app.config['SECRET_KEY'] = 'LKSFJ*S(*DJF#)@F#L@KJD'
app.config['UPLOAD_FOLDER'] = '/mnt/hgfs/Devnet_share/Uploads'


class FormData(FlaskForm):
    username = StringField('Email/Phone')
    password = PasswordField('Secret Key')


class CrawlFormData(FlaskForm):
    device_models = [('4510', 'Cat 4500'), ('3850', 'Cat 3800'), ('3560', 'Cat 3560'), ('2960', 'Cat 2960')]
    hostname = StringField('Enter the Hostname and Select config file : ')
    device_model = SelectField("Select the device model : ", choices=device_models, default=1)
    submit = SubmitField("Crraawwl")


class UploadFormData(FlaskForm):
    submit = SubmitField("Crraawwl")


# Declare the table
class ItemTable(Table):
    short_int = Col('Interface')
    sw_port_mode = Col('Mode')
    status = Col('Status')
    data_vlan = Col('Data Vlan')
    voice_vlan = Col('Voice Vlan')
    data_flag = Col('Data Flag')
    voice_flag = Col('Voice Flag')
    description = Col('Description')
    mac_data = Col('MAC')
    DeviceID = Col('DeviceID')
    IPAddress = Col('IP Add')
    Platform = Col('Platform')
    Capabilities = Col('Capabilities')

'''
@app.route('/form', methods=['GET', 'POST'])
def form():
    form_data = FormData()
    if form_data.validate_on_submit():
        return '<h1> The Username is : {}, The Secret Key is: {}'.format(form_data.username.data, form_data.password.data)
    return render_template('form.html', form=form_data)
'''


@app.route('/crawl_form', methods=['GET', 'POST'])
def crawl_form():
    form = CrawlFormData()
    if form.validate_on_submit():
        device_info = {}
        all_output = []
        file_handle = request.files['file']
        if file:
            filename = secure_filename(file_handle.filename)
            file_handle.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            a = "File uploaded Successfully !"
        device_info['device'] = form.hostname.data
        device_info['device_type'] = form.device_model.data
        output = constructor(device_info, False)
        all_output.append(output)
        return render_template('output.html', coreoutput=all_output, title="Switch Information")
    return render_template('crawler_form1.html', form=form)


@app.route('/upload_crawl_form', methods=['GET', 'POST'])
def upload_crawl_form():
    form = UploadFormData()
    if form.validate_on_submit():
        device_info = {}
        all_output = []
        if request.files['legacy_new_file']:
            file_handle = request.files['legacy_new_file']
            filename = secure_filename(file_handle.filename)
            file_handle.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            port_info = True
        else:
            port_info = False
        for single_file in request.files.getlist('files'):
            filename = secure_filename(single_file.filename)
            single_file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            device_info['device'] = filename
            output = constructor(device_info, port_info)
            all_output.append(output)
        return render_template('output.html', coreoutput=all_output, title="Switches Information")
    return render_template('crawler_form.html', form=form)


if __name__ == '__main__':
    app.run(debug=True)
